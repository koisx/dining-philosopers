package com.devx;

public class Philosopher implements Runnable {

    private final Object leftFork;
    private final Object rightFork;
    private static final int TIMES_TO_THINK = 10;

    Philosopher(Object left, Object right) {
        this.leftFork = left;
        this.rightFork = right;
    }

    private void doAction(String action) throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + " " + action);
        Thread.sleep(((int) (Math.random() * 100)));
    }

    @Override
    public void run() {
        try {
            for (int i=0; i<TIMES_TO_THINK; ++i) {
                doAction(System.nanoTime() + "--> Думаю..."); // thinking
                synchronized (leftFork) {
                    doAction(System.nanoTime() + "--> Взяв ліву виделку");
                    synchronized (rightFork) {
                        doAction(System.nanoTime() + "--> Взяв праву виделку => буду їсти"); // eating
                        doAction(System.nanoTime() + "--> Закінчив їсти => кладу праву виделку");
                    }
                    doAction(System.nanoTime() + "--> Кладу ліву виделку");
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
